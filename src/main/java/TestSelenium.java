

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestSelenium {
    public static Object randomNumber(){
        int max = 9999;
        int min = 10;
        Random rand = new Random();
        int value = rand.nextInt((max - min) + 1) + min;
        return value;
    }

    public static void main(String[] args) {
        FirefoxDriver driver = new FirefoxDriver();
        //переход на страницу https://www.youtube.com/
        driver.get("http://youtube.com/");

        //проверить, что вкладка браузера называется “YouTube”
        assertTrue(driver.getTitle().contains("YouTube"));

        //ввод в строку поиска запрос(запрос должен состоять из 2-4 цифр, случайных при каждом запуске теста)
        driver.findElement(By.name("search_query")).click();
        driver.findElement(By.name("search_query")).sendKeys("" + randomNumber());

        //сделать браузер во весь экран
        driver.manage().window().maximize();

        //в списке результатов поиска выбираем вторую позицию сверху
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sbse1")));
        element.click();

        //запуск четвертого сверху видео в списке результатов поиска
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ytd-video-renderer[4]")));
        element1.click();

        //клик на аватар отправителя видео
        WebElement element2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ytd-video-owner-renderer/a")));
        element2.click();

        //клик на кнопку с текстом “ПОДПИСАТЬСЯ”
        WebElement element3 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/ytd-app/div/ytd-page-manager/ytd-browse[2]/div[3]/ytd-c4-tabbed-header-renderer/app-header-layout/div/app-header/div[2]/div[2]/div/div[1]/div/div[2]/div[3]/ytd-button-renderer")));
        element3.click();

        // проверка, что текст “ВОЙТИ”(точное совпадение) отображается в появившемся контейнере
        WebElement element4 = driver.findElement(By.xpath("/html/body/ytd-app/ytd-popup-container/iron-dropdown[1]/div/ytd-modal-with-title-and-button-renderer/div/ytd-button-renderer/a"));
        String textString = element4.getText();
        System.out.println(textString);
        assertEquals("ВОЙТИ", textString);

        //закрыть вкладку браузера
        driver.close();
        }
    }